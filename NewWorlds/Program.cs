﻿using System;

using SFML.Graphics;
using SFML.Window;

namespace NewWorlds
{
    static class NewWorlds
    {
        private static source.Game game;

        static void Main()
        {
            var x = uint.Parse(source.settings.Config.ReadSetting("Window_X"));
            var y = uint.Parse(source.settings.Config.ReadSetting("Window_Y"));
            var fps = uint.Parse(source.settings.Config.ReadSetting("FPS_Limit"));

            RenderWindow window = new RenderWindow(new VideoMode(x, y), "New Worlds", Styles.Close | Styles.Titlebar);
            window.SetFramerateLimit(fps);

            game = new source.Game(window);

            window.Closed += new EventHandler(OnClosed);
            window.KeyPressed += new EventHandler<KeyEventArgs>(OnEvent);
            window.MouseMoved += new EventHandler<MouseMoveEventArgs>(OnEvent);
            window.MouseButtonPressed += new EventHandler<MouseButtonEventArgs>(OnMousePressEvent);
            window.MouseButtonReleased += new EventHandler<MouseButtonEventArgs>(OnMouseReleaseEvent);
            window.MouseWheelMoved += new EventHandler<MouseWheelEventArgs>(OnEvent);

            game.GameLoop();
        }
        
        static void OnClosed(object sender, EventArgs e)
        {
            RenderWindow window = (RenderWindow)sender;
            window.Close();
        }
        static void OnMousePressEvent(object sender, MouseButtonEventArgs e)
        {
            OnEvent(sender, MouseButtonPressedEventArgs.FromBase(e));
        }
        static void OnMouseReleaseEvent(object sender, MouseButtonEventArgs e)
        {
            OnEvent(sender, MouseButtonReleasedEventArgs.FromBase(e));
        }
        static void OnEvent(object sender, EventArgs e)
        {
            RenderWindow window = (RenderWindow)sender;

            game.HandleInput(e);
        }

        /* Manually disambiguate between the two */
        public class MouseButtonPressedEventArgs : MouseButtonEventArgs
        {
            public static MouseButtonPressedEventArgs FromBase(MouseButtonEventArgs mbea)
            {
                var mbe = new MouseButtonEvent();
                mbe.Button = mbea.Button;
                mbe.X = mbea.X;
                mbe.Y = mbea.Y;

                return new MouseButtonPressedEventArgs(mbe);
            }

            public MouseButtonPressedEventArgs(MouseButtonEvent e)
                :base(e)
            {

            }
        }
        public class MouseButtonReleasedEventArgs : MouseButtonEventArgs
        {
            public static MouseButtonReleasedEventArgs FromBase(MouseButtonEventArgs mbea)
            {
                var mbe = new MouseButtonEvent();
                mbe.Button = mbea.Button;
                mbe.X = mbea.X;
                mbe.Y = mbea.Y;

                return new MouseButtonReleasedEventArgs(mbe);
            }

            public MouseButtonReleasedEventArgs(MouseButtonEvent e)
                :base(e)
            {

            }
        }
    }
}
