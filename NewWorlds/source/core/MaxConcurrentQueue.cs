﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewWorlds.source.core
{
    public class MaxConcurrentQueue<T>
    {
        private readonly object privLock = new object();

        readonly ConcurrentQueue<T> queue = new ConcurrentQueue<T>();

        public int Size { get; private set; }

        public MaxConcurrentQueue(int size)
        {
            Size = size;
        }

        public T Aggregate(Func<T, T, T> func)
        {
            return Enumerable.Aggregate(queue, func);
        }

        public void Enqueue(T obj)
        {
            queue.Enqueue(obj);

            lock (privLock)
            {
                while (queue.Count > Size)
                {
                    T outObj;
                    queue.TryDequeue(out outObj);
                }
            }
        }
    }
}
