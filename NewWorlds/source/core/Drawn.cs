﻿using SFML.Graphics;

namespace NewWorlds.source.core
{
    public abstract class Drawn : Transformable, Drawable
    {
        public virtual void Update(long dt)
        {

        }
        public abstract void Draw(RenderTarget target, RenderStates states);
        public virtual void Draw(RenderTarget target, RenderStates states, long dt)
        {
            Update(dt);
            Draw(target, states);
        }
    }
}
