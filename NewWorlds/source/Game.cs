﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Threading;

using SFML.Graphics;
using SFML.System;
using SFML.Window;

namespace NewWorlds.source
{
    using states;
    class Game
    {
        private RenderWindow window;
        private texture.TextureManager textureManager;

        private map.TileAtlas tileTypes;
        private Stack<State> states;
        private Queue<StateChange> changes;

        private static uint tileSize = 256;
        private bool texturesLoaded = false;
        
        public void PushState(State st)
        {
            states.Push(st);
        }

        private void What(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        public void PopState()
        {
            states.Pop();
        }
        public State Top()
        {
            return states.First();
        }

        public void GameLoop()
        {
            var style = new gui.GUIStyle();
            var state = RenderStates.Default;

            LoadTiles();

            var offset = 16;
            var rect = new IntRect(offset, (int)tileSize / 2 + offset, (int)tileSize - 2 * offset, (int)tileSize - 2 * offset);
            textureManager.TileTexture("void", "background", window.Size.X, window.Size.Y, rect);

            Clock clock = new Clock();
            Debug.WriteLine("[GAME] Launching New Worlds.");

            var mm = new states.menu.MainMenu();
            PushState(mm);

            while (window.IsOpen)
            {
                var elapsed = clock.Restart();

                window.DispatchEvents();

                HandleChanges();

                window.Clear(style.GetColor(0));

                Top().UpdateState(elapsed.AsMicroseconds());
                Top().Draw(window, state, elapsed.AsMicroseconds());

                window.Display();
            }
        }

        public void HandleChanges()
        {
            while (changes.Count > 0)
            {
                var sc = changes.Dequeue();

                Debug.WriteLine("[GAME] State change: " + sc.newState);
                if (sc.newState == typeof(ExitState))
                {
                    window.Close();
                }
                else if (sc.newState == typeof(PopState))
                {
                    states.Pop();
                }
                else
                {
                    if (sc.GetType() == typeof(StateSet))
                    {
                        states.Clear();
                    }
                    var param = new List<object>();
                    if (sc.newState.IsSubclassOf(typeof(TextureState)))
                    {
                        param.Add(textureManager);
                    }
                    if (sc.newState == typeof(GameState))
                    {
                        var nmap = map.MapFactory.GenerateVoronoi(tileSize, 64, 64, tileTypes);
                        param.Add(nmap);
                        param.Add((Vector2f)window.Size);
                    }
                    states.Push((State)Activator.CreateInstance(sc.newState, param.ToArray()));
                }
            }
        }

        public void HandleInput(EventArgs e)
        {
            var topState = Top();
            var paramT = new Type[] { window.GetType(), e.GetType() };
            var hiMethod = topState.GetType().GetMethod("HandleInput", paramT);
            var sc = (StateChange)hiMethod.Invoke(topState, new object[] { window, e });
            if (sc != null)
            {
                changes.Enqueue(sc);
            }
        }

        private void LoadTextures()
        {
            textureManager.AddTextureTo("void", "tiles/void.png", 4);
            textureManager.AddTextureTo("desert", "tiles/desertDunes.png", 4);
            textureManager.AddTextureTo("dirt", "tiles/dirt.png", 4);
            textureManager.AddTextureTo("forest", "tiles/forestBroadleaf.png", 4);
            textureManager.AddTextureTo("hills", "tiles/hills.png", 4);
            textureManager.AddTextureTo("marsh", "tiles/marsh.png", 4);
            textureManager.AddTextureTo("mountains", "tiles/mountain.png", 4);
            textureManager.AddTextureTo("ocean", "tiles/ocean.png", 4);
            textureManager.AddTextureTo("plains", "tiles/plains.png", 4);

            /* road_[NSEW] connections */
            textureManager.AddTextureTo("road_0101", "tiles/roads/road_corner0101-.png", 2);
            textureManager.AddTextureTo("road_0110", "tiles/roads/road_corner0110-.png", 2);
            textureManager.AddTextureTo("road_1001", "tiles/roads/road_corner1001-.png", 2);
            textureManager.AddTextureTo("road_1010", "tiles/roads/road_corner1010-.png", 2);
            textureManager.AddTextureTo("road_1111", "tiles/roads/road_crossroads1111-.png", 4);
            textureManager.AddTextureTo("road_0001", "tiles/roads/road_end_short0001-.png", 1);
            textureManager.AddTextureTo("road_0010", "tiles/roads/road_end_short0010-.png", 1);
            textureManager.AddTextureTo("road_0100", "tiles/roads/road_end_short0100-.png", 1);
            textureManager.AddTextureTo("road_1000", "tiles/roads/road_end_short1000-.png", 1);
            textureManager.AddTextureTo("road_0001", "tiles/roads/road_end0001-.png", 1);
            textureManager.AddTextureTo("road_0010", "tiles/roads/road_end0010-.png", 1);
            textureManager.AddTextureTo("road_0100", "tiles/roads/road_end0100-.png", 1);
            textureManager.AddTextureTo("road_1000", "tiles/roads/road_end1000-.png", 1);
            textureManager.AddTextureTo("road_0011", "tiles/roads/road_straight0011-.png", 2);
            textureManager.AddTextureTo("road_1100", "tiles/roads/road_straight1100-.png", 2);
            textureManager.AddTextureTo("road_0111", "tiles/roads/road_tee0111-.png", 2);
            textureManager.AddTextureTo("road_1011", "tiles/roads/road_tee1011-.png", 2);
            textureManager.AddTextureTo("road_1101", "tiles/roads/road_tee1101-.png", 2);
            textureManager.AddTextureTo("road_1110", "tiles/roads/road_tee1110-.png", 2);
        }
        private void LoadTiles()
        {
            if (!texturesLoaded)
            {
                LoadTextures();
                texturesLoaded = true;
            }

            var height = 1.5F;
            var animList = new List<texture.Animation>()
            {
                new texture.Animation(0, 0, 1.0F),
                new texture.Animation(0, 0, 1.0F),
                new texture.Animation(0, 0, 1.0F),
                new texture.Animation(0, 0, 1.0F)
            };

            tileTypes[map.TileType.PLAINS] =
                new map.Tile(tileSize, height, textureManager.GetTexture("plains"),
                animList,
                map.TileType.PLAINS);

            tileTypes[map.TileType.FOREST] =
                new map.Tile(tileSize, height, textureManager.GetTexture("forest"),
                animList,
                map.TileType.FOREST);

            tileTypes[map.TileType.DESERT] =
                new map.Tile(tileSize, height, textureManager.GetTexture("desert"),
                animList,
                map.TileType.DESERT);

            tileTypes[map.TileType.MOUNTAINS] =
                new map.Tile(tileSize, height, textureManager.GetTexture("mountains"),
                animList,
                map.TileType.MOUNTAINS);

            tileTypes[map.TileType.DIRT] =
                new map.Tile(tileSize, height, textureManager.GetTexture("dirt"),
                animList,
                map.TileType.DIRT);

            tileTypes[map.TileType.HILLS] =
                new map.Tile(tileSize, height, textureManager.GetTexture("hills"),
                animList,
                map.TileType.HILLS);

            tileTypes[map.TileType.MARSH] =
                new map.Tile(tileSize, height, textureManager.GetTexture("marsh"),
                animList,
                map.TileType.MARSH);

            tileTypes[map.TileType.OCEAN] =
                new map.Tile(tileSize, height, textureManager.GetTexture("ocean"),
                animList,
                map.TileType.OCEAN);

            for (int i = 1; i < 15; i++)
            {
                string cur = Convert.ToString(i, 2).PadLeft(4, '0');
                tileTypes[map.TileType.ROAD, cur] =
                new map.Tile(tileSize, height, textureManager.GetTexture("road_" + cur),
                animList,
                map.TileType.ROAD);
            }
        }

        public Game(RenderWindow window)
        {
            this.window = window;

            textureManager = new texture.TextureManager();
            tileTypes = new map.TileAtlas();

            states = new Stack<State>();
            changes = new Queue<StateChange>();
        }
    }
}
