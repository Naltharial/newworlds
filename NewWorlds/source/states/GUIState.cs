﻿using System;
using System.Diagnostics;
using System.Collections.Generic;

using SFML.Graphics;
using SFML.System;
using SFML.Window;

namespace NewWorlds.source.states
{
    abstract class GUIState : State
    {
        /* Close the GUI by default */
        public virtual StateChange HandleInput(RenderWindow window, KeyEventArgs e)
        {
            if (e.Code == Keyboard.Key.Escape)
            {
                return new StateChange(typeof(PopState));
            }
            else
            {
                return null;
            }
        }

        public override void Draw(RenderTarget target, RenderStates states)
        {
            foreach (var gui in guiSystem)
            {
                if (!gui.Value.Visible)
                {
                    continue;
                }

                var size = gui.Value.GetSize();
                var px = target.Size.X / 2 - size.X / 2;
                var py = target.Size.Y / 2 - size.Y / 2;
                gui.Value.Position = new Vector2f(px, py);

                target.Draw(gui.Value);
            }
        }
    }
}
