﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SFML.Graphics;
using SFML.Window;
using SFML.System;

namespace NewWorlds.source.states
{
    enum ActionState { NONE, PANNING, SELECTING };

    class GameState : TextureState, IDisposable
    {
        public map.Map map
        {
            get;
            protected set;
        }
        
        private Sprite backSprite;
        private ActionState actionState;

        private View gameView;
        private View guiView;

        private Vector2i panningAnchor;
        private float zoomLevel;

        private Vector2i selectionStart;
        private Vector2i selectionEnd;

        private map.Tile currentTile;

        public void Center(Vector2f pos)
        {

            guiView.Size = pos;
            gameView.Size = pos;
            pos *= 0.5f;
            guiView.Center = pos;
            zoomLevel = 1.0F;

            var centre = new Vector2f(map.width * 0.5F, map.height * 0.5F);
            centre *= map.tileSize;
            gameView.Center = centre;
        }

        public override void Update(long dt)
        {
            map.Update(dt);
        }
        public override void Draw(RenderTarget target, RenderStates states)
        {
            target.SetView(guiView);
            target.Draw(backSprite, states);

            target.SetView(gameView);
            map.Draw(target, states);

            target.SetView(guiView);
            DrawFPS(target, states);
        }

        #region Event Handling
        public StateChange HandleInput(RenderWindow window, KeyEventArgs e)
        {
            if (e.Code == Keyboard.Key.Escape)
            {
                return new StateChange(typeof(states.menu.ExitDialog));
            }
            else
            {
                return null;
            }
        }
        public StateChange HandleInput(RenderWindow window, MouseMoveEventArgs e)
        {
            if (actionState == ActionState.PANNING)
            {
                var lim = new Vector2f(map.width, map.height);
                lim *= map.tileSize;
                
                var mctp = window.MapCoordsToPixel(gameView.Center);
                Vector2f pos = (Vector2f)(Mouse.GetPosition(window) - panningAnchor);

                if (pos.X < 0 && mctp.X >= lim.X)
                {
                    pos.X = 0;
                }
                if (pos.X > 0 && mctp.X < 0)
                {
                    pos.X = 0;
                }
                if (pos.Y < 0 && mctp.Y >= lim.Y)
                {
                    pos.Y = 0;
                }
                if (pos.Y > 0 && mctp.Y < 0)
                {
                    pos.Y = 0;
                }

                gameView.Move(-1.0F * pos * zoomLevel);
                panningAnchor = Mouse.GetPosition(window);
            }

            return null;
        }
        public StateChange HandleInput(RenderWindow window, NewWorlds.MouseButtonPressedEventArgs e)
        {
            if (e.Button == Mouse.Button.Middle)
            {
                if (actionState != ActionState.PANNING)
                {
                    actionState = ActionState.PANNING;
                    panningAnchor = Mouse.GetPosition(window);
                }
            }
            else if (e.Button == Mouse.Button.Right)
            {
                actionState = ActionState.NONE;
                map.ClearSelected();
            }

            return null;
        }
        public StateChange HandleInput(RenderWindow window, NewWorlds.MouseButtonReleasedEventArgs e)
        {
            if (e.Button == Mouse.Button.Middle)
            {
                actionState = ActionState.NONE;
            }
            else if (e.Button == Mouse.Button.Left)
            {
                if (actionState == ActionState.SELECTING)
                {
                    actionState = ActionState.NONE;
                }
            }

            return null;
        }
        public StateChange HandleInput(RenderWindow window, MouseWheelEventArgs e)
        {
            if (e.Delta < 0)
            {
                if (zoomLevel > 8) return null;
                gameView.Zoom(1.5f);
                zoomLevel *= 1.5f;
            }
            else
            {
                if (zoomLevel < 0.5) return null;
                gameView.Zoom(0.75f);
                zoomLevel *= 0.75f;
            }
            Debug.WriteLine("[GAMESTATE] Altering zoom delta to: " + zoomLevel);

            return null;
        }
        #endregion

        public new void Dispose()
        {
            ((IDisposable)backSprite).Dispose();
            ((IDisposable)gameView).Dispose();
            ((IDisposable)guiView).Dispose();
        }

        private GameState(texture.TextureManager textureManager)
            : base(textureManager)
        {
            var tx = textureManager.GetTexture("background");
            backSprite = new Sprite(tx);

            selectionStart = new Vector2i(0, 0);
            selectionEnd = new Vector2i(0, 0);

            actionState = ActionState.NONE;

            guiView = new View();
            gameView = new View();
        }

        public GameState(texture.TextureManager textureManager, map.Map map, Vector2f size)
            : this(textureManager)
        {
            this.map = map;

            currentTile = map.tileAtlas[source.map.TileType.PLAINS];
            backSprite.Scale = new Vector2f(
                size.X / backSprite.GetLocalBounds().Width,
                size.Y / backSprite.GetLocalBounds().Height);

            Center(size);
        }
    }
}
