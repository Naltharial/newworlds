﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewWorlds.source.states
{
    abstract class TextureState : State
    {
        protected texture.TextureManager textureManager;

        public TextureState(texture.TextureManager textureManager)
            :base()
        {
            this.textureManager = textureManager;
        }
    }
}
