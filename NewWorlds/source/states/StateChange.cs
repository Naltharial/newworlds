﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewWorlds.source.states
{
    public class StateChange
    {
        public Type newState;

        public StateChange(Type ns)
        {
            newState = ns;
        }
    }
}
