﻿using System;

using SFML.Graphics;

namespace NewWorlds.source.states
{
    interface IState
    {
        StateChange HandleInput(RenderWindow window, EventArgs e);

        void Update(long dt);
    }
}
