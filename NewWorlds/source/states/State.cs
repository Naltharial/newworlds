﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;

using SFML.Graphics;
using SFML.System;

namespace NewWorlds.source.states
{
    abstract class State : core.Drawn, IState
    {
        private core.MaxConcurrentQueue<double> fps;
        private Text fpsText;
        private readonly int sampleSize = 100;

        protected Dictionary<string, gui.GUI> guiSystem;
        protected long ElapsedTime
        {
            get;
            private set;
        }

        public virtual StateChange HandleInput(RenderWindow window, EventArgs e)
        {
            return null;
        }

        public double GetFPS()
        {
            return fps.Aggregate((a,b) => a + b) / fps.Size;
        }

        public void UpdateState(long dt)
        {
            ElapsedTime += dt;
            fps.Enqueue(1/(dt/1000000.0));

            Update(dt);
        }

        public virtual void DrawFPS(RenderTarget target, RenderStates states)
        {
            fpsText.DisplayedString = ((uint)GetFPS()).ToString();
            var lb = fpsText.GetLocalBounds();
            fpsText.Position = new Vector2f(target.Size.X - 30 - lb.Width, 20);

            fpsText.Draw(target, states);
        }

        public override void Update(long dt)
        {

        }

        public State()
        {
            guiSystem = new Dictionary<string, gui.GUI>();
            
            fpsText = new Text("", gui.GUI.style.bodyFont, 20);
            fpsText.Color = Color.White;
            
            fps = new core.MaxConcurrentQueue<double>(sampleSize);
        }
    }
}
