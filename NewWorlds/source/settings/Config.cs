﻿using System;
using System.Configuration;
using System.Diagnostics;

namespace NewWorlds.source.settings
{
    class Config
    {
        public static string ReadSetting(string key)
        {
            Debug.WriteLine("[CONFIG] Reading setting key: " + key);
            try
            {
                var appSettings = ConfigurationManager.AppSettings;
                string result = appSettings[key] ?? "Not Found";
                Debug.WriteLine("[CONFIG] Got setting: " + result);
                return result;
            }
            catch (ConfigurationErrorsException e)
            {
                Debug.WriteLine("[CONFIG] Error reading settings: " + e);
                return "Error";
            }
        }
    }
}
