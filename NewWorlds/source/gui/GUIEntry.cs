﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SFML.Graphics;

namespace NewWorlds.source.gui
{
    class GUIEntry
    {
        public RectangleShape shape;
        public string message;
        public Text text;

        public static string CutToSize(string check, Font font, uint size, float width, char separator = '\n')
        {
            FloatRect ins;
            int ln = 0;
            string rinstr;
            do
            {
                var tok = check.Split(separator);
                var tc = tok.Length;
                while (tc < ln)
                {
                    rinstr = tok.Aggregate("", (max, cur) => max.Length > cur.Length ? max : cur);
                    var ltok = rinstr.Split(' ');

                    var nwidth = width + 1;
                    var ilen = ltok.Length;
                    while (nwidth > width)
                    {
                        var tokins = new Text(string.Join(" ", new ArraySegment<string>(ltok, 0, ilen--)), font, size);
                        ins = tokins.GetLocalBounds();
                        nwidth = ins.Width;

                        if (ilen == 0)
                        {
                            ilen = 1;
                            nwidth = 0;
                        }
                    }
                    var a1s = string.Join(" ", new ArraySegment<string>(ltok, 0, ilen));
                    var a2s = string.Join(" ", new ArraySegment<string>(ltok, ilen, ltok.Length - ilen));
                    var ind = Array.IndexOf(tok, rinstr);

                    tok[ind] = a1s;
                    var tokl = tok.ToList();
                    tokl.Insert(ind + 1, a2s);
                    tok = tokl.ToArray();
                    tc = tok.Length;
                }
                rinstr = string.Join("\n", tok);
                var instructions = new Text(rinstr, font, size);
                ins = instructions.GetLocalBounds();
                ln++;
            } while (ins.Width > width);

            return rinstr;
        }

        public GUIEntry()
        {

        }

        public GUIEntry(string message, RectangleShape shape, Text text)
            :this()
        {
            this.message = message;
            this.shape = shape;
            this.text = text;
        }
    }
}
