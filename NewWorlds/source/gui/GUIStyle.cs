﻿using System;
using System.Collections.Generic;

using SFML.Graphics;
using SFML.Window;
using SFML.System;

namespace NewWorlds.source.gui
{
    public sealed class GUIStyle : IDisposable
    {
        public Font bodyFont;
        public Font titleFont;

        public uint titleSize = 22;
        public uint itemSize = 18;
        public uint bodySize = 16;

        public int border = 4;

        private List<Color> cgp;

        public GUIStyle()
        {
            bodyFont = new Font("resources/fonts/xolonium/Xolonium-Regular.otf");
            titleFont = new Font("resources/fonts/cgf_locust_resistance/CGF Locust Resistance.ttf");

            cgp = new List<Color>();
            cgp.Add(new Color(64, 128, 128));
            cgp.Add(new Color(164, 228, 228));
            cgp.Add(new Color(255, 255, 255));
        }

        public void Dispose()
        {
            bodyFont.Dispose();
            titleFont.Dispose();
        }

        public Color GetColor(int index, byte alpha=255)
        {
            var cr =  new Color(cgp[index]);
            cr.A = alpha;

            return cr;
        }
    }
}
