﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SFML.Graphics;
using SFML.System;

namespace NewWorlds.source.gui
{
    class GUI : core.Drawn
    {
        public static readonly GUIStyle style = new GUIStyle();

        private bool horizontal;
        public bool Horizontal
        {
            get
            {
                return horizontal;
            }
            set
            {
                horizontal = value;

                Title = title;
                Instructions = instructions;
            }
        }

        private Vector2f position;
        public new Vector2f Position
        {
            get
            {
                return position;
            }
            set
            {
                position = value;

                if (!string.IsNullOrEmpty(Title))
                {
                    titleText.Position = Position;
                }
                if (!string.IsNullOrEmpty(Instructions))
                {
                    instrText.Position = Position;
                }

                foreach (var entry in entries)
                {
                    entry.shape.Position = Position;
                    entry.text.Position = Position;
                }
            }
        }

        private Vector2f dimensions;
        public Vector2f Dimensions
        {
            get
            {
                return dimensions;
            }
            set
            {
                dimensions = value;

                foreach (var entry in entries)
                {
                    entry.shape.Size = value;
                    entry.text.CharacterSize = style.itemSize;
                }
            }
        }

        private bool visible;
        public bool Visible
        {
            get
            {
                return visible;
            }
            set
            {
                if (value)
                {
                    var texts = 0.0F;
                    if (!string.IsNullOrEmpty(Title))
                    {
                        texts += titleText.GetLocalBounds().Height;
                        texts += style.border * 2;
                    }
                    var offset = new Vector2f(0.0F, texts);
                    if (!string.IsNullOrEmpty(Instructions))
                    {
                        if (!string.IsNullOrEmpty(Title))
                        {
                            instrText.Origin = -offset;
                        }
                        texts += instrText.GetLocalBounds().Height;
                        texts += style.border * 2;
                    }
                    offset.Y = texts;

                    foreach (var entry in entries)
                    {
                        entry.shape.Origin = Origin - offset;

                        var ts = entry.text.GetLocalBounds();
                        var xoffs = (entry.shape.Size.X - 2 * style.border) / 2 - ts.Width / 2 + style.border;
                        entry.text.Origin = Origin - offset - new Vector2f(xoffs, 0);
                        
                        if (Horizontal) offset.X += dimensions.X;
                        else offset.Y += dimensions.Y;
                    }
                }

                visible = value;
            }
        }

        private string title = "";
        private Text titleText;
        public string Title
        {
            get
            {
                return title;
            }
            set
            {
                var rtit = GUIEntry.CutToSize(value, style.titleFont, style.titleSize, GetSize().X);
                titleText = new Text(rtit, style.bodyFont, style.bodySize);
                titleText.Color = style.GetColor(1);
                
                title = value;
                Visible = true;
            }
        }

        private string instructions = "";
        private Text instrText;
        public string Instructions
        {
            get
            {
                return instructions;
            }
            set
            {
                var rinstr = GUIEntry.CutToSize(value, style.bodyFont, style.bodySize, GetSize().X);
                instrText = new Text(rinstr, style.bodyFont, style.bodySize);
                instrText.Color = style.GetColor(1);

                instructions = value;
                Visible = true;
            }
        }

        public List<GUIEntry> entries;
        
        public Vector2f GetSize()
        {
            var texts = 0.0F;
            if (!string.IsNullOrEmpty(Title))
            {
                texts += titleText.GetLocalBounds().Height;
                texts += style.border * 2;
            }
            if (!string.IsNullOrEmpty(Instructions))
            {
                texts += instrText.GetLocalBounds().Height;
                texts += style.border * 2;
            }

            if (Horizontal)
            {
                return new Vector2f(dimensions.X * entries.Count, dimensions.Y + texts);
            }
            else
            {
                return new Vector2f(dimensions.X, dimensions.Y * entries.Count + texts);
            }
        }

        public int GetEntryAt(Vector2f mousePos)
        {
            if (entries.Count == 0) return -1;
            if (!visible) return -1;

            for (int i = 0; i < entries.Count; ++i)
            {
                Vector2f point = mousePos;
                point += entries[i].shape.Origin;
                point -= entries[i].shape.Position;

                if (point.X < 0 || point.X > entries[i].shape.Scale.X * dimensions.X) continue;
                if (point.Y < 0 || point.Y > entries[i].shape.Scale.Y * dimensions.Y) continue;
                return i;
            }

            return -1;
        }

        public void SetEntryText(int entry, string text)
        {
            entries[entry].text.DisplayedString = text;
        }

        public override void Draw(RenderTarget target, RenderStates states)
        {
            if (!visible) return;

            if (!string.IsNullOrEmpty(Title))
            {
                target.Draw(titleText);
            }

            if (!string.IsNullOrEmpty(Instructions))
            {
                target.Draw(instrText);
            }

            foreach (var entry in entries)
            {
                target.Draw(entry.shape);
                target.Draw(entry.text);
            }
        }

        public void Highlight(int entry)
        {
            for (int i = 0; i < entries.Count; ++i)
            {
                if (i == entry && entries[i].message != "")
                {
                    entries[i].shape.FillColor = style.GetColor(1);
                    entries[i].shape.OutlineColor = style.GetColor(2);
                    entries[i].text.Color = style.GetColor(2);
                }
                else
                {
                    entries[i].shape.FillColor = style.GetColor(0);
                    entries[i].shape.OutlineColor = style.GetColor(1);
                    entries[i].text.Color = style.GetColor(1);
                }
            }
        }

        public string Activate(int entry)
        {
            if (entry == -1) return "null";
            return entries[entry].message;
        }
        public string Activate(Vector2f mousePos)
        {
            int entry = GetEntryAt(mousePos);
            return Activate(entry);
        }
        public string Activate(float x, float y)
        {
            return Activate(new Vector2f(x, y));
        }

        public GUI()
        {
            visible = false;

            entries = new List<GUIEntry>();
        }
        public GUI(Vector2f dimensions, List<Tuple<string, string>> entries)
            :this()
        {
            this.dimensions = dimensions;

            var shape = new RectangleShape();
            shape.Size = dimensions;
            shape.FillColor = style.GetColor(0);
            shape.OutlineThickness = -style.border;
            shape.OutlineColor = style.GetColor(1);

            foreach (var entry in entries)
            {
                var text = new Text();
                text.DisplayedString = entry.Item1;
                text.Font = style.bodyFont;
                text.Color = style.GetColor(1);
                text.CharacterSize = style.itemSize;

                this.entries.Add(new GUIEntry(entry.Item2, new RectangleShape(shape), text));
            }

        }
    }
}
