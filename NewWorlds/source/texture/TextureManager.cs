﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SFML.Graphics;
using SFML.System;

namespace NewWorlds.source.texture
{
    class TextureManager
    {
        private Dictionary<string, Texture> textures;
        private Dictionary<string, List<string>> texlib;

        public Texture AddTextureTo(string name, string filename)
        {
            var path = "resources/" + filename;
            if (!File.Exists(path))
            {
                throw new FileNotFoundException("Texture file '" + path + "' does not exist.");
            }
            List<string> lelem;
            var ex = texlib.TryGetValue(name, out lelem);
            if (!ex)
            {
                texlib[name] = new List<string>();
            }
            texlib[name].Add(filename);
            textures.Remove(name);

            return GetTexture(name);
        }
        public Texture AddTextureTo(string name, string filename, uint num)
        {
            string ext = Path.GetExtension(filename);
            string bs = Path.GetFileNameWithoutExtension(filename);
            string dir = Path.GetDirectoryName(filename);
            if (!string.IsNullOrEmpty(dir))
            {
                dir += "/";
            }

            for (uint i = 0; i < num; i++)
            {
                var path = string.Format("resources/{0}{1}{2,2:00}{3}", dir, bs, i, ext);
                if (!File.Exists(path))
                {
                    throw new FileNotFoundException("Texture file '" + path + "' does not exist in resources/.");
                }
                List<string> lelem;
                var ex = texlib.TryGetValue(name, out lelem);
                if (!ex)
                {
                    texlib[name] = new List<string>();
                }
                texlib[name].Add(path);
            }

            return GetTexture(name);
        }

        public Texture TileTexture(string texture, string name, uint x, uint y, IntRect txSrc)
        {
            var compt = GetTexture(texture);
            var compti = compt.CopyToImage();
            var numt = texlib[texture].Count;
            var ts = new Vector2u((uint)(compt.Size.X / numt), compt.Size.Y);
            var xtile = x / txSrc.Width + (x % txSrc.Width > 0 ? 1 : 0);
            var ytile = y / txSrc.Height + (y % txSrc.Height > 0 ? 1 : 0);

            var rand = new Random();
            Image compi = new Image(x, y);
            var basel = txSrc.Left;
            for (uint i = 0; i < ytile; i++)
            {
                for (uint j = 0; j < xtile; j++)
                {
                    txSrc.Left = basel + (int)(ts.X * rand.Next(0, numt));
                    compi.Copy(compti, (uint)txSrc.Width * j, (uint)txSrc.Height * i, txSrc);
                }
            }

            var tex = new Texture(compi);
            textures[name] = tex;

            return tex;
        }
        public Texture TileTexture(string texture, string name, uint x, uint y)
        {
            var compt = GetTexture(texture);
            var compti = compt.CopyToImage();
            var numt = texlib[texture].Count;
            var ts = new Vector2i((int)(compt.Size.X / numt), (int)compt.Size.Y);
            var intr = new IntRect(0, 0, ts.X, ts.Y);

            return TileTexture(texture, name, x, y, intr);
        }

        public Texture this[string name]
        {
            get { return GetTexture(name); }
        }

        public Texture GetTexture(string name)
        {
            Texture tex;
            var cached = textures.TryGetValue(name, out tex);
            if (!cached)
            {
                var list = texlib[name];
                Image compi = null;
                for (int i = 0; i < list.Count; i++)
                {
                    var curi = new Image(list[i]);
                    if (compi == null)
                    {
                        compi = new Image(curi.Size.X, (uint)(curi.Size.Y * list.Count));
                    }
                    compi.Copy(curi, 0, (uint)(curi.Size.Y * i));
                }

                tex = new Texture(compi);
                textures[name] = tex;
            }

            return tex;
        }
        public bool TryGetTexture(string name, out Texture texture)
        {
            var success = false;
            try
            {
                texture = GetTexture(name);
                success = true;
            }
            catch (KeyNotFoundException)
            {
                texture = null;
            }

            return success;
        }

        public void Cull()
        {
            /*  Only remove the dynamically loaded
                textures, we can't know how to
                refresh the others.                 */
            foreach (var item in texlib)
            {
                Texture tx;
                var succ = textures.TryGetValue(item.Key, out tx);
                if (succ)
                {
                    tx.Dispose();
                }
            }
            textures = new Dictionary<string, Texture>();
        }

        public TextureManager()
        {
            textures = new Dictionary<string, Texture>();
            texlib = new Dictionary<string, List<string>>();
        }
    }
}
