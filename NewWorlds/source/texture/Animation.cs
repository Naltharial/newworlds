﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewWorlds.source.texture
{
    public struct Animation
    {
        public uint startFrame;
        public uint stopFrame;

        public float duration;

        public uint GetFrameNumber()
        {
            return stopFrame - startFrame + 1;
        }

        public Animation(uint startFrame, uint stopFrame, float duration)
        {
            this.startFrame = startFrame;
            this.stopFrame = stopFrame;
            this.duration = duration;
        }
    }
}
