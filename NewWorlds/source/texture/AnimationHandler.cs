﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SFML.Graphics;

namespace NewWorlds.source.texture
{
    public class AnimationHandler
    {
        private List<Animation> animations;

        public float currentTime = 0.0F;
        public int currentFrame = -1;

        public int Count
        {
            get
            {
                return animations.Count;
            }
        }

        public IntRect Bounds
        {
            get;
            set;
        }
        public IntRect Size
        {
            get;
            set;
        }

        public IEnumerable<Animation> GetAnimations()
        {
            return animations;
        }

        public void AddAnimation(Animation animation)
        {
            animations.Add(animation);
        }
        public void ChangeAnimation(int number)
        {
            if (currentFrame == number || number >= animations.Count ||
                number < 0) return;

            currentFrame = number;
            IntRect rect = Size;
            rect.Top = rect.Height * number;
            Bounds = rect;
            currentTime = 0.0F;
        }

        public void Update(long dt)
        {
            if (currentFrame >= animations.Count || currentFrame < 0)
            {
                return;
            }

            float duration = animations[currentFrame].duration;
            if ((int)((currentTime + dt) / duration) > (int)(currentTime / duration))
            {
                int frame = (int)((currentTime + dt) / duration);
                
                frame %= (int)animations[currentFrame].GetFrameNumber();
                
                IntRect rect = Size;
                rect.Left = rect.Width * frame;
                rect.Top = rect.Height * currentFrame;
                Bounds = rect;
            }
            
            currentTime += dt;
            if (currentTime > duration * animations[currentFrame].GetFrameNumber())
            {
                currentTime = 0.0f;
            }
        }

        public AnimationHandler()
        {
            animations = new List<Animation>();
        }
    }
}
