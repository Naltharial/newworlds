﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenSimplex;

namespace NewWorlds.source.map.terrain
{
    class OpenSimplexProvider
    {
        private double persistence;

        private Random rand;
        private List<OpenSimplexNoise> osne;

        private void FillEngines(uint octaves)
        {
            for (int i = 0; i < octaves; i++)
            {
                osne.Add(new OpenSimplexNoise(rand.Next()));
            }
        }

        public double GetNoise(uint x, uint y)
        {
            var result = 0.0;
            for (int i = 0; i < osne.Count; i++)
            {
                result += osne[i].Evaluate(x / Math.Pow(2, i), y / Math.Pow(2, i)) * Math.Pow(persistence, osne.Count - i);
            }

            return result;
        }

        public OpenSimplexProvider(uint octaves)
        {
            rand = new Random();
            osne = new List<OpenSimplexNoise>();

            persistence = 0.5;

            FillEngines(octaves);
        }
        public OpenSimplexProvider(uint octaves, double persistence)
            :this(octaves)
        {
            this.persistence = persistence;
        }

        public OpenSimplexProvider(uint octaves, double persistence, int seed)
        {
            rand = new Random(seed);
            osne = new List<OpenSimplexNoise>();

            this.persistence = persistence;

            FillEngines(octaves);
        }
    }
}
