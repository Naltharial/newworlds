﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewWorlds.source.map.terrain
{
    class GraphData
    {
        public csDelaunay.Site Site
        {
            get;
            set;
        }

        private List<double> heightData;
        private List<double> moistureData;

        public double GetHeight()
        {
            return heightData.Average();
        }
        public double GetMoisture()
        {
            return moistureData.Average();
        }

        public void AddHeight(double point)
        {
            heightData.Add(point);
        }
        public void AddMoisture(double point)
        {
            moistureData.Add(point);
        }

        public GraphData()
        {
            heightData = new List<double>();
            moistureData = new List<double>();
        }
    }
}
