﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewWorlds.source.map.terrain
{
    class DelunayProvider
    {
        public csDelaunay.Voronoi GetMap(uint width, uint height)
        {
            var list = new List<csDelaunay.Vector2f>();
            var rand = new Random();

            for (int i = 0; i < width * height; i++)
            {
                list.Add(new csDelaunay.Vector2f(rand.NextDouble() * width, rand.NextDouble() * height));
            }

            var bounds = new csDelaunay.Rectf(0, 0, width, height);

            var vor = new csDelaunay.Voronoi(list, bounds, 3);

            return vor;
        }
    }
}
