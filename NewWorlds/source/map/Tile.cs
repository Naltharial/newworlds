﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using SFML.Graphics;
using SFML.System;

namespace NewWorlds.source.map
{
    enum TileType
    {
        VOID, DESERT, PLAINS, FOREST, HILLS, MOUNTAINS, OCEAN, DIRT, ROAD,
        MARSH
    }

    class Tile : core.Drawn, IDisposable
    {
        public texture.AnimationHandler animationHandler;
        public Sprite sprite;
        
        public int TileVariant
        {
            get;
            set;
        }
        public TileType tileType;
        
        public uint[] regions;

        public double moisture;
        public double height;

        public override void Update(long dt)
        {
            ResetGraphic();

            animationHandler.Update(dt);

            sprite.TextureRect = animationHandler.Bounds;
        }
        public override void Draw(RenderTarget target, RenderStates states)
        {
            ResetGraphic();

            sprite.Draw(target, states);
        }

        public new void Dispose()
        {
            sprite.Dispose();
        }

        public void RandomGraphic()
        {
            var rand = new Random();
            TileVariant = rand.Next(animationHandler.Count);
        }
        public void RandomGraphic(int seed)
        {
            var rand = new Random(seed);
            TileVariant = rand.Next(animationHandler.Count);
        }

        private void ResetGraphic()
        {
            animationHandler.ChangeAnimation(TileVariant);
        }

        public Tile()
        {
            animationHandler = new texture.AnimationHandler();
            sprite = new Sprite();
            regions = new uint[1];
        }
        public Tile(uint tileSize, float height, Texture texture, List<texture.Animation> animations, TileType tileType)
            :this()
        {
            this.tileType = tileType;

            sprite.Origin = new Vector2f(0.0F, tileSize * (height - 1));
            sprite.Texture = texture;
            animationHandler.Size = new IntRect(0, 0, (int)tileSize, (int)(tileSize * height));
            animationHandler.Bounds = animationHandler.Size;

            foreach (var animation in animations)
            {
                animationHandler.AddAnimation(animation);
            }
            animationHandler.Update(0);
        }

        public Tile(Tile tile)
            :this()
        {
            tileType = tile.tileType;

            sprite.Origin = tile.sprite.Origin;
            sprite.Texture = tile.sprite.Texture;
            animationHandler.Size = tile.animationHandler.Size;
            animationHandler.Bounds = tile.animationHandler.Bounds;

            foreach (var animation in tile.animationHandler.GetAnimations())
            {
                animationHandler.AddAnimation(animation);
            }
            animationHandler.Update(0);
        }
    }

    struct TileData
    {
        public TileType type;

        public int tileVariant;
        public double moisture;
        public double height;

        public static TileData ExtractData(Tile tile)
        {
            var td = new TileData();

            td.type = tile.tileType;

            td.tileVariant = tile.TileVariant;
            td.moisture = tile.moisture;
            td.height = tile.height;

            return td;
        }
    }
}
