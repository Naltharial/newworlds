﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewWorlds.source.map.city
{
    class City : IDisposable
    {
        public CityMap map;

        public City()
        {
            
        }

        void Update(long dt)
        {
            map.Update(dt);
        }

        void TileChanged()
        {
            map.UpdateDirection(TileType.ROAD);
            map.FindConnectedRegions(new List<TileType>() { TileType.ROAD }, 0);
        }

        public void Dispose()
        {
            map.Dispose();
        }

        public City(string cityName, uint tileSize, Dictionary<TileType, Tile> tileAtlas)
            : this()
        {
            map = new CityMap(tileSize, tileAtlas);
        }
    }
}
