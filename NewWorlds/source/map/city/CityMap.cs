﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewWorlds.source.map.city
{
    class CityMap : Map
    {
        public CityMap(uint tileSize, IDictionary<TileType, Tile> tileAtlas)
            :base(tileSize, tileAtlas)
        {

        }
    }
}
