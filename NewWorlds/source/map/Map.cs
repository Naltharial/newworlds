﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SFML.System;
using SFML.Graphics;

namespace NewWorlds.source.map
{
    class Map : core.Drawn
    {
        public uint width;
        public uint height;
        
        public List<Tile> tiles;
        public IDictionary<TileType, Tile> tileAtlas;

        public List<int> resources;

        public uint tileSize;
        public uint[] numRegions;

        private Tile selected;

        public override void Update(long dt)
        {
            for (int y = 0; y < height; ++y)
            {
                for (int x = 0; x < width; ++x)
                {
                    /* Update and re-extract data */
                    tiles[(int)(y * width + x)].Update(dt);
                }
            }
        }

        public override void Draw(RenderTarget target, RenderStates states)
        {
            for (int y = 0; y < height; ++y)
            {
                for (int x = 0; x < width; ++x)
                {
                    Vector2f pos = new Vector2f();
                    
                    pos.X = x * tileSize;
                    pos.Y = y * tileSize;

                    var tile = tiles[(int)(y * width + x)];
                    tile.sprite.Position = pos;

                    tile.Draw(target, states);
                }
            }
        }

        public void UpdateDirection(TileType tileType)
        {
            for (int y = 0; y < height; ++y)
            {
                for (int x = 0; x < width; ++x)
                {
                    int pos = (int)(y * width + x);

                    if (tiles[pos].tileType == tileType)
                        UpdateDirection((uint)x, (uint)y);
                }
            }
        }

        public void UpdateDirection(uint x, uint y)
        {
            int pos = (int)(y * width + x);

            var tile = tiles[pos];
            bool[,] adjacentTiles = new bool[3,2] {{ false, false },{ false, false },{ false, false } };
            
            if(x > 0 && y > 0)
                adjacentTiles[0,0] = (tiles[(int)((y - 1) * width + (x - 1))].tileType == tile.tileType);
            if(y > 0)
                adjacentTiles[0,3] = (tiles[(int)((y - 1) * width + (x))].tileType == tile.tileType);
            if(x< width-1 && y> 0)
                adjacentTiles[0,4] = (tiles[(int)((y - 1) * width + (x + 1))].tileType == tile.tileType);
            if(x > 0)
                adjacentTiles[1,0] = (tiles[(int)((y) * width + (x - 1))].tileType == tile.tileType);
            if(x<width-1)
                adjacentTiles[1,5] = (tiles[(int)((y) * width + (x + 1))].tileType == tile.tileType);
            if(x > 0 && y< height-1)
                adjacentTiles[2,0] = (tiles[(int)((y + 1) * width + (x - 1))].tileType == tile.tileType);
            if(y< height-1)
                adjacentTiles[2,6] = (tiles[(int)((y + 1) * width + (x))].tileType == tile.tileType);
            if(x< width-1 && y< height-1)
                adjacentTiles[2,7] = (tiles[(int)((y + 1) * width + (x + 1))].tileType == tile.tileType);
            
            if(adjacentTiles[1,0] && adjacentTiles[1,8] && adjacentTiles[0,9] && adjacentTiles[2,10])
                tiles[pos].TileVariant = 2;
            else if(adjacentTiles[1,0] && adjacentTiles[1,11] && adjacentTiles[0,12])
                tiles[pos].TileVariant = 7;
            else if(adjacentTiles[1,0] && adjacentTiles[1,13] && adjacentTiles[2,14])
                tiles[pos].TileVariant = 8;
            else if(adjacentTiles[0,15] && adjacentTiles[2,16] && adjacentTiles[1,0])
                tiles[pos].TileVariant = 9;
            else if(adjacentTiles[0,16] && adjacentTiles[2,17] && adjacentTiles[1,18])
                tiles[pos].TileVariant = 10;
            else if(adjacentTiles[1,0] && adjacentTiles[1,19])
                tiles[pos].TileVariant = 0;
            else if(adjacentTiles[0,20] && adjacentTiles[2,21])
                tiles[pos].TileVariant = 1;
            else if(adjacentTiles[2,22] && adjacentTiles[1,0])
                tiles[pos].TileVariant = 3;
            else if(adjacentTiles[0,23] && adjacentTiles[1,24])
                tiles[pos].TileVariant = 4;
            else if(adjacentTiles[1,0] && adjacentTiles[0,25])
                tiles[pos].TileVariant = 5;
            else if(adjacentTiles[2,26] && adjacentTiles[1,27])
                tiles[pos].TileVariant = 6;
            else if(adjacentTiles[1,0])
                tiles[pos].TileVariant = 0;
            else if(adjacentTiles[1,28])
                tiles[pos].TileVariant = 0;
            else if(adjacentTiles[0,29])   
                tiles[pos].TileVariant = 1;
            else if(adjacentTiles[2,30])
                tiles[pos].TileVariant = 1;
        }

        public void ClearSelected()
        {
            selected = null;
        }

        public void FindConnectedRegions(List<TileType> whitelist, int regionType = 0)
        {
            int regions = 1;

            foreach (var tile in tiles) tile.regions[regionType] = 0;

            for (int y = 0; y < height; ++y)
            {
                for (int x = 0; x < width; ++x)
                {
                    bool found = false;
                    foreach (var type in whitelist)
                    {
                        if (type == tiles[(int)(y * width + x)].tileType)
                        {
                            found = true;
                            break;
                        }
                    }
                    if (tiles[(int)(y * width + x)].regions[regionType] == 0 && found)
                    {
                        DepthFirstSearch(whitelist, new Vector2i(x, y), regions++, regionType);
                    }
                }
            }
            numRegions[regionType] = (uint)regions;
        }

        private void DepthFirstSearch(List<TileType> whitelist, Vector2i pos, int label, int regionType = 0)
        {
            if (pos.X < 0 || pos.X >= width) return;
            if (pos.Y < 0 || pos.Y >= height) return;
            if (tiles[(int)(pos.Y * width + pos.X)].regions[regionType] != 0) return;

            bool found = false;
            foreach (var type in whitelist)
            {
                if (type == tiles[(int)(pos.Y * width + pos.X)].tileType)
                {
                    found = true;
                    break;
                }
            }
            if (!found) return;

            tiles[(int)(pos.Y * width + pos.X)].regions[regionType] = (uint)label;

            DepthFirstSearch(whitelist, pos + new Vector2i(-1, 0), label, regionType);
            DepthFirstSearch(whitelist, pos + new Vector2i(0, 1), label, regionType);
            DepthFirstSearch(whitelist, pos + new Vector2i(1, 0), label, regionType);
            DepthFirstSearch(whitelist, pos + new Vector2i(0, -1), label, regionType);
        }

        public Map(uint tileSize, IDictionary<TileType, Tile> tileAtlas)
        {
            this.tileSize = tileSize;

            tiles = new List<Tile>();
            resources = new List<int>();
            numRegions = new uint[1];

            this.tileAtlas = tileAtlas;
        }
    }
}
