﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewWorlds.source.map
{
    static class MapFactory
    {
        private static uint osoct = 6;
        private static double osper = 0.6;
        private static uint vsample = 4;

        private static Map GenerateFrame(uint tileSize, uint width, uint height, IDictionary<TileType, Tile> tileAtlas)
        {
            var rand = new Random();
            var map = new Map(tileSize, tileAtlas);

            var prov = new terrain.DelunayProvider();
            prov.GetMap(width, height);

            map.width = width;
            map.height = height;

            for (int i = 0; i < map.width * map.height; i++)
            {
                var rind = rand.Next(0, tileAtlas.Count);
                var el = tileAtlas.ElementAt(rind).Value;
                map.tiles.Add(el);
            }

            return map;
        }

        public static Map GenerateRandom(uint tileSize, uint width, uint height, IDictionary<TileType, Tile> tileAtlas)
        {
            return GenerateFrame(tileSize, width, height, tileAtlas);
        }

        public static Map GenerateSea(uint tileSize, uint width, uint height, IDictionary<TileType, Tile> tileAtlas)
        {
            var nd = new Dictionary<TileType, Tile>();
            nd[TileType.OCEAN] = tileAtlas[TileType.OCEAN];
            return GenerateFrame(tileSize, width, height, tileAtlas);
        }

        public static Map GenerateGrassland(uint tileSize, uint width, uint height, IDictionary<TileType, Tile> tileAtlas)
        {
            var nd = new Dictionary<TileType, Tile>();
            nd[TileType.PLAINS] = tileAtlas[TileType.PLAINS];
            return GenerateFrame(tileSize, width, height, nd);
        }

        public static Map GenerateVoronoi(uint tileSize, uint width, uint height, IDictionary<TileType, Tile> tileAtlas)
        {
            /* Random Generation */
            var genMap = new Map(tileSize, tileAtlas);
            genMap.height = height;
            genMap.width = width;

            var dp = new terrain.DelunayProvider();
            var ospH = new terrain.OpenSimplexProvider(osoct, osper);
            var ospM = new terrain.OpenSimplexProvider((uint)(osoct * 1.25), osper);
            var rand = new Random();
            var result = dp.GetMap(width, height);

            int n = 0;
            foreach (var item in result.SitesIndexedByLocation)
            {
                var i = (uint)(n / height);
                var j = (uint)(n % width);

                var gd = new terrain.GraphData();

                for (uint k = 0; k < Math.Pow(vsample, 2); k++)
                {
                    uint l = i * vsample + (k % vsample);
                    uint m = j * vsample + (k / vsample);

                    var osn = (ospH.GetNoise(l, m) + 1) / 2;
                    gd.AddHeight(osn);
                    var osm = (ospM.GetNoise(l, m) + 1) / 2;
                    gd.AddMoisture(osm);
                }

                var tile = new Tile(tileAtlas[DetermineBiome(gd.GetHeight(), gd.GetMoisture())]);

                tile.moisture = gd.GetMoisture();
                tile.height = gd.GetHeight();
                tile.RandomGraphic(rand.Next());

                genMap.tiles.Add(tile);
                
                n++;
            }

            return genMap;
        }

        private static TileType DetermineBiome(double e, double m)
        {
            if (e > 0.7)
            {
                return TileType.MOUNTAINS;
            }

            if (e > 0.55)
            {
                if (m < 0.33) return TileType.DESERT;
                return TileType.HILLS;
            }

            if (m < 0.22) return TileType.DESERT;
            if (m < 0.56) return TileType.PLAINS;
            if (m < 0.74) return TileType.FOREST;
            if (m < 0.80) return TileType.MARSH;
            return TileType.OCEAN;
        }
    }
}
