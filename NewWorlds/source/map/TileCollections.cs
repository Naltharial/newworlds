﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewWorlds.source.map
{
    sealed class TileList : IList<Tile>
    {
        IList<TileData> items;

        public IDictionary<TileType, Tile> tileAtlas;

        public int Count
        {
            get { return items.Count; }
        }

        public void CopyTo(Tile[] array, int index)
        {
            Array.Copy(Items.ToArray(), index, array, 0, items.Count - index);
        }

        public bool Contains(Tile item)
        {
            return items.Contains(TileData.ExtractData(item));
        }

        public int IndexOf(Tile item)
        {
            return items.IndexOf(TileData.ExtractData(item));
        }

        public IEnumerable<Tile> Items
        {
            get
            {
                foreach (var td in items)
                {
                    yield return ConvertData(td);
                }
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return items.IsReadOnly;
            }
        }

        public Tile this[int index]
        {
            get
            {
                return ConvertData(items[index]);
            }
            set
            {
                var td = TileData.ExtractData(value);
                items[index] = td;
            }
        }

        public void Insert(int index, Tile item)
        {
            var td = TileData.ExtractData(item);
            items.Insert(index, td);
        }

        public void RemoveAt(int index)
        {
            items.RemoveAt(index);
        }

        public void Add(Tile item)
        {
            var td = TileData.ExtractData(item);
            items.Add(td);
        }

        public void Clear()
        {
            items.Clear();
        }

        public bool Remove(Tile item)
        {
            var td = TileData.ExtractData(item);
            return items.Remove(td);
        }

        public IEnumerator<Tile> GetEnumerator()
        {
            return Items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        private Tile ConvertData(TileData data)
        {
            var tl = tileAtlas[data.type];

            tl.TileVariant = data.tileVariant;

            tl.height = data.height;
            tl.moisture = data.moisture;

            return tl;
        }

        public TileList(IDictionary<TileType, Tile> tileAtlas)
        {
            items = new List<TileData>();

            this.tileAtlas = tileAtlas;
        }

        public TileList(IDictionary<TileType, Tile> tileAtlas, IList<Tile> list)
            : this(tileAtlas)
        {
            foreach (var t in list)
            {
                items.Add(TileData.ExtractData(t));
            }
        }
    }

    sealed class TileAtlas : IDictionary<TileType, Tile>
    {
        private IDictionary<TileType, IDictionary<string, Tile>> typeClass;

        private string defaultSpace = "void";

        public Tile this[TileType key]
        {
            get
            {
                return typeClass[key][defaultSpace];
            }

            set
            {
                Add(key, value);
            }
        }
        public Tile this[TileType key, string space]
        {
            get
            {
                return typeClass[key][space];
            }

            set
            {
                Add(key, space, value);
            }
        }

        public int Count
        {
            get
            {
                return typeClass.Count;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        public ICollection<TileType> Keys
        {
            get
            {
                return typeClass.Keys;
            }
        }

        public ICollection<Tile> Values
        {
            get
            {
                return (ICollection<Tile>)typeClass.SelectMany(d => d.Value.Values).ToList();
            }
        }

        public void Add(KeyValuePair<TileType, Tile> item)
        {
            Add(item.Key, item.Value);
        }

        public void Add(TileType key, Tile value)
        {
            Add(key, defaultSpace, value);
        }
        public void Add(TileType key, string space, Tile value)
        {
            if (!typeClass.ContainsKey(key))
            {
                var ke = new Dictionary<string, Tile>();
                typeClass.Add(key, ke);
            }

            typeClass[key].Add(space, value);
        }

        public void Clear()
        {
            typeClass = new Dictionary<TileType, IDictionary<string, Tile>>();
        }

        public bool Contains(KeyValuePair<TileType, Tile> item)
        {
            return typeClass[item.Key].Contains(new KeyValuePair<string, Tile>(defaultSpace, item.Value));
        }
        
        public bool ContainsKey(TileType key)
        {
            return typeClass.ContainsKey(key);
        }

        public void CopyTo(KeyValuePair<TileType, Tile>[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public IEnumerator<KeyValuePair<TileType, Tile>> GetEnumerator()
        {
            var sr = typeClass.Select(d => new KeyValuePair<TileType, Tile>(d.Key, d.Value[defaultSpace]));
            return sr.GetEnumerator();
        }

        public bool Remove(KeyValuePair<TileType, Tile> item)
        {
            throw new NotImplementedException();
        }

        public bool Remove(TileType key)
        {
            return typeClass.Remove(key);
        }

        public bool TryGetValue(TileType key, out Tile value)
        {
            value = null;
            IDictionary<string, Tile> ov;
            var res = typeClass.TryGetValue(key, out ov);
            return res && ov.TryGetValue(defaultSpace, out value);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public TileAtlas()
        {
            Clear();
        }
    }
}
